INTRODUCTION
------------

This is a module that allows the Stacktable javascript library to be used via
Libraries API module.

For a full description of the module, visit the project page:
https://www.drupal.org/project/stacktable_lib

To submit bug reports and feature suggestions, or to track changes:
https://www.drupal.org/project/issues/stacktable_lib

Stacktable is an open source jQuery extension with source available on github.
Please post any bugs related to Stacktable.js on github:
https://github.com/johnpolacek/stacktable.js/issues


REQUIREMENTS
------------
This module requires the following modules:

* Libraries API (https://drupal.org/project/libraries)


INSTALLATION
------------

1. Download the latest version of the Stacktable library from:
http://johnpolacek.github.io/stacktable.js/

2. Extract the library to a library directory named stacktablejs. For example if
you are using the default libraries location the js file will be at
sites/all/libraries/stacktablejs/stacktable.js

3. Remove unneeded files and folders from stacktablejs directory (optional).
You really only need to keep stacktable.css and stacktable.js and optionally
README.md and stacktable.jquery.json.

4. Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.
